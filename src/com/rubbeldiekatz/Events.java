package com.rubbeldiekatz;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Felix on 09-Oct-16.
 * <p>
 * Event Listener.
 *
 * TODO BLACKLIST for blocks in config
 */
class Events implements Listener {

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerUse(PlayerInteractEvent event) {
        try {
            playerUse(event);
        } catch (Exception e) {
            e.printStackTrace();
            // getLogger().info(e.getStackTrace());
        }
    }

    private void playerUse(PlayerInteractEvent event) {
        buildersWandRightClick(event);
    }

    private void buildersWandRightClick(PlayerInteractEvent event) {
        Block clickedBlock = event.getClickedBlock();
        Player p = event.getPlayer();
        if (!checkForWand(p)) {
            return;
        }
        if (clickedBlock == null) {
            return;
        }
        p.sendMessage("Clicked Block: " + clickedBlock.getType().toString());
        BuildersWand.log("Clicked Block: " + clickedBlock.getType().toString());
        // At this point, the player has the Builder's Wand in his hand.
        // Try to place all the blocks.
        tryToPlaceBlocks(event);
        p.sendMessage(ChatColor.AQUA + "Event fired through!");
    }

    private void tryToPlaceBlocks(PlayerInteractEvent event) {
        Player p = event.getPlayer();
        BlockFace face = event.getBlockFace();
        p.sendMessage("Placing Blocks!");
        p.sendMessage(face.toString());
        Block adjacent = event.getClickedBlock().getRelative(face);
        if (!blockCanBeReplaced(adjacent)) {
            // Players may not place blocks through gaps (chests etc.)
            return;
        }
        // If the above are both true, we will continue placing blocks around this one.
        // Don't forget adjacent too.
        p.sendMessage("Starting to place blocks...");
        placeBlock(adjacent, event.getClickedBlock(), p);
    }

    private void placeBlock(Block block, Block placedAgainst, Player p) {
        // DONE Check for Build permission
        ItemStack heldItem = p.getInventory().getItemInMainHand();
        EquipmentSlot slot = EquipmentSlot.HAND;
        if (!blockCanBeReplaced(block)) {
            return;
        }
        if (!blockMayBePlaced(placedAgainst)) {
            return;
        }
        BlockPlaceEvent e = new BlockPlaceEvent(block, block.getState(), placedAgainst, heldItem, p, true, slot);
        if (e.isCancelled()) {
            return;
        }
        block.breakNaturally();
        block.setType(placedAgainst.getType());
    }

    private boolean blockMayBePlaced(Block b) {

        Material m = b.getType();
        ArrayList<Material> blocked = new ArrayList<>();
        blocked.add(Material.CHEST);
        blocked.add(Material.TRAPPED_CHEST);
        return !blocked.contains(m);
    }

    private boolean checkForWand(Player p) {
        ItemStack heldItem = p.getInventory().getItemInMainHand();
        ItemMeta meta = heldItem.getItemMeta();
        if (meta == null) {
            // TODO remove this in final version
            giveBuildersWand(p);
            return false;
        }
        List<String> lore = meta.getLore();
        if (lore == null) {
            // TODO remove this in final version
            giveBuildersWand(p);
            return false;
        }
        if (heldItem.getType() != Material.STICK) {
            return false;
        }
        if (!lore.contains("Builder's Wand")) {
            // TODO remove this in final version
            giveBuildersWand(p);
            return false;
        }
        return true;
    }

    private void giveBuildersWand(Player p) {
        if (p.hasPermission("builderswand.give")) {
            ItemStack stack = new ItemStack(Material.STICK, 1);
            ItemMeta im = stack.getItemMeta();
            im.setDisplayName("Testing item");
            im.setLore(Collections.singletonList("Builder's Wand"));
            stack.setItemMeta(im);

            p.getInventory().addItem(stack);
        }
    }

    private boolean blockCanBeReplaced(Block b) {
        // TODO WHITELIST for blocks that can be replaced (air, liquid, grass etc)
        if (!b.isLiquid()) {
            // Break if not liquid...
            if (!b.isEmpty()) {
                // ... or empty.
                return false;
            }
        }
        return true;
    }
}
