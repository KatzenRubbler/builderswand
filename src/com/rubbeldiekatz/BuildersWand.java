package com.rubbeldiekatz;

import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

/**
 * Created by Felix on 09-Oct-16.
 *
 * Main class.
 */
public class BuildersWand extends JavaPlugin {

    private static Logger logger;

    public static void log(String s) {
        logger.info(s);
    }

    @Override
    public void onEnable() {
        //Fired when the server enables the plugin
        logger = getLogger();
        log(String.format("Enabled Version %s", getDescription().getVersion()));
        getServer().getPluginManager().registerEvents(new Events(), this);
    }

    @Override
    public void onDisable() {
        //Fired when the server stops and disables all plugins
    }
}
